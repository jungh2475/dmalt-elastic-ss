#

import gspread
import pandas as pd
from oauth2client.service_account import ServiceAccountCredentials


class Gsheet(object):



    def __init__(self, sheet_id='1EogyCrngPhZAcxTCA7WCD6c94GLfdAXHlgNrxtCrWVY'):
        self.scope=['https://spreadsheets.google.com/feeds']
        self.credentials = ServiceAccountCredentials.from_json_keyfile_name('My Project 1131-92e23d331383.json', self.scope)
        self.sheet_id=sheet_id
        self.subsheet_name=""
        gc = gspread.authorize(self.credentials)
        self.sheets=gc.open_by_key(sheet_id)
    
    def load_gsheets(self):
        return self.sheets
    
    def load_subsheet(self, subsheet_name):
        self.subsheet_name = subsheet_name
        return self.sheets.worksheet(subsheet_name)
    

if __name__ == '__main__':
    
    sheet_id='1EogyCrngPhZAcxTCA7WCD6c94GLfdAXHlgNrxtCrWVY'  #1EogyCrngPhZAcxTCA7WCD6c94GLfdAXHlgNrxtCrWVY, or kt.crawler.assignments.201610 #sheet_url='https://docs.google.com/spreadsheets/d/1EogyCrngPhZAcxTCA7WCD6c94GLfdAXHlgNrxtCrWVY/edit#gid=0'
    subsheet_name='assignments'
    
    #gspread쓰기 귀찮으면 csv로 webPublish해도 됨: https://docs.google.com/spreadsheets/d/e/2PACX-1vQ9H0m_TMjJZBabGWIUP2ZUV4iYVEWh5OQKx56dRf3tLsH7W6LYgegR6oOcv_Ckwqg2CVneJHGom4j5/pub?gid=0&single=true&output=csv
    #pd.read_csv('https://docs.google.com/spreadsheets/d/e/2PACX-1vQ9H0m_TMjJZBabGWIUP2ZUV4iYVEWh5OQKx56dRf3tLsH7W6LYgegR6oOcv_Ckwqg2CVneJHGom4j5/pub?gid=0&single=true&output=csv', columns=['aaa','bbb'])  #header=None, skiprows=[1]
    #my_sheet=pd.read_csv(sheet_url, header=0, index_col=0, skiprows=[2])
    #gsheet_id
    #gsheet_scope
    #gsheet_credentialjsonfile # from http://console.developers.google.com/ > credentials > serviceAccount, http://gspread.readthedocs.io/en/latest/oauth2.html
    #gsheet_subsheetName
    #serviceAccount : 867067161628-compute@developer.gserviceaccount.com -> 반드시 문서에서 접속이 되도록 해놓는다. api도 접속 가능하게 ....
    #credentials = ServiceAccountCredentials.from_json_keyfile_name(gsheet_credentialjsonfile, gsheet_scope)
    #wks=gspread.authorize(credentials).open_by_key(gsheet_id).worksheet(gsheet_subsheetName)
    #df=pd.DataFrame(wks.get_all_records(), columns=wks.get_all_values()[0])
    
    
    
    
    #argument가 있는 경우 import sys, a=sys.argv[1]
    
    my_sheetsObj=Gsheet(sheet_id)
    my_sub_sheet=my_sheetsObj.load_subsheet(subsheet_name)
    wks=my_sheetsObj.load_subsheet(subsheet_name)
    #temp_value=my_sub_sheet.cell(5,2).value
    #print(temp_value)
    
    #pd.read_csv()
    data = wks.get_all_records()
    cols = wks.get_all_values()[0] #keeps cols in right order
    df = pd.DataFrame(data,columns=cols)
    
    
    '''
    pandas to sql : df.to_sql('table_name',engine), engine = sqlalchemy.create_engine(connstring)
    '''