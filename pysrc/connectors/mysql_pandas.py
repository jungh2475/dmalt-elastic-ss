'''
Created on Jun 12, 2017
pip show MySQL-python.... or.... sudo apt-get install python-mysqldb
alternatively .pip install PyMySQL ....

@author: jungh
'''


import pandas as pd
import MySQLdb


class DataLoader(object):
    
    def __init__(self, host, userid, password, dbname):
        self.host=host
        self.userid=userid
        self.password=password
        self.dbname=dbname;
        self.db
        self.query
        self.df
    
    def load(self, query):
        #kwargs는 딕셔너리 형태로 전달
        self.db=MySQLdb.connect(host=self.host, user=self.userid, password=self.password, db=self.dbname)    
        self.query=query
        self.df=pd.read_sql(self.query,self.db) # 옛날 방법 .... self.df=pd.io.sql.read_sql(self.query,self.db) ... from pandas.io import sql
        return self.df


if __name__ == "__main__":
    
    kargs={'host':'localhost','userid':'root','password':'1234','dbname':'kew2db'}  #dictionary
    dataloader=DataLoader(**kargs)  
    ddf=dataloader.load()
    print(ddf)
    