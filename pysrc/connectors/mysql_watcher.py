'''
Created on Jun 12, 2017

    tables : user, user_record(request), => sysrecord, eventrecord => content.type=report  
    
    http://ljs93kr.tistory.com/59 
    pip install flask, flask_sqlalchemy, pymysql....pip install flask-sqlalchemy mysql-python
    https://scotch.io/tutorials/build-a-crud-web-app-with-python-and-flask-part-one
@author: jungh


    다른 곳에서 쓰는 방법
    from connectors import mysqldb1
    
    db1=MySqlDb()
    db1.table......

'''

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
# import pymysql  #import MySQLdb

#이것이 필요한가요?.....
try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass


app =  Flask(__name__)  #app = Flask(__name__, instance_relative_config=True)
app.config['SQLALCHEMY_DATABASE_URI']='mysql://root:1234@localhost/pyalchemy'
app.config['SQLALCHEMY_ECHO'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


#from app import db, db.create_all()


class User(db.Model):
    __tablename__ ='users'
    __table_args__={'mysql_collate':'utf8_general_ci'}
    id=db.Column('id', db.Integer, primary_key=True, unique=True)  #db.String(30), db.Boolean, default=False
    username=db.Column('username',db.Unicode)
    
    def __init__(self,id_,user_name):
        self.id=id_
        self.username=user_name
    
    def __repr__(self):
        return 'user_id:%s, user_name:%s' %(self.id, self.username)
    
    def as_dict(self):
        return { x.name:getattr(self, x.name) for x in self.__table__.columns }

class UserRecord(db.Model):  #type=watcher_request
    __tablename__ ='userrecord'
    id=db.Column('id', db.Integer, primary_key=True)
    data=db.Column('data',db.Unicode)
    
    def __init__(self,id_,data):
        self.id=id_
        self.data=data
        
class SysRecord(db.Model):  #type=watcher_request
    __tablename__ ='sysrecord'
    id=db.Column('id', db.Integer, primary_key=True)
    data=db.Column('data',db.Unicode)
    
    def __init__(self,id_,data):
        self.id=id_
        self.data=data

class EventRecord(db.Model):  #type=watcher_request
    __tablename__ ='eventrecords'
    id=db.Column('id', db.Integer, primary_key=True)
    data=db.Column('data',db.Unicode)
    
    def __init__(self,id_,data):
        self.id=id_
        self.data=data

class WatcherContent(db.Model): #type=watcher_report
    __tablename__ ='sysrecord'
    id=db.Column('id', db.Integer, primary_key=True)
    data=db.Column('data',db.Unicode)
    
    def __init__(self,id_,data):
        self.id=id_
        self.data=data






if __name__ == "__main__":
    
    #examples=Example.query.all()... or one=Example.query.filter_by(id=1).first()...db.session.add(Example(x,x))
    pass