'''

usage example)
    from mysql_dmalt import *  그리고 User, UserRecord 등을 사용하시믄 됩니다. 
        class(table) : user, record(order,quote,), inventory(person, device, tag)
                    content(product, deliverypath=approval,circulation,) sysrecord, userrecord, actionrecord
                    log, transactionRecord, comment, article, blog, .... 

Created on Jun 13, 2017

@author: jungh

'''

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='mysql://root:1234@localhost/kew3db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
db=SQLAlchemy(app)

            
'''
    임의의 table에서 필요한 기능들은...insert/update/delete, read.db.Model을 상속해서 필요없음 
    
'''    
class Product(db.Model):
        __tablename__ ='products'
        
        id=db.Column('id', db.Integer, primary_key=True, unique=True)
        name=db.Column('name',db.String(25)) #db.Unicode
        supplier=db.Column('supplier',db.Unicode)
        tag=db.Column('tag',db.Unicode)
    
        def __init__(self,id_,name_,supplier_,tag_):
            self.id=id_
            self.name=name_
            self.supplier=supplier_
            self.tag=tag_

#class User(db.Model):
#class UserRecord(db.Model):  #watcher-tracking,
#class WatcherContent(db.Model):  #Report(tracking)

#class Inventory(db.Model):  #Device, Tag/Keyword/Collection
#class SysRecord(db.Model):            #Log
#class EventRecord(db.Model):    #progress-report, task, crawl-assignment, parse-assignment

#class EsContent(db.Model):  #ERP



if __name__ == "__main__":
    
    #Product.query.count(), len(products)
    
    products=Product.query.all()
    for product in products:
        print(product.name)
    
    pass