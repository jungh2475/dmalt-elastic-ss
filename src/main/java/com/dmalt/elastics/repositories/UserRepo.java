package com.dmalt.elastics.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dmalt.elastics.models.User;


public interface UserRepo extends JpaRepository<User, Long> {

}
