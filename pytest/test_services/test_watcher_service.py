'''

usage_example) 
        python3 test_watcher.py
        (여러개의 테스트 한꺼번에 돌리리) python3 -m unittest test_module1 test_module2
        (test method1개만 돌려보기) python3 -m test_watcher.py TestModule1.test_methodName
        python -m unittest -v test_module.TestClass, python -m unittest tests/test_something.py

exception testing)

using mock)
    - from py3.3 or pip install mock(py2.7)
    - MagicMock(return_value=x), @mock.patch('Rm.os')..def(self,objj)
    - @mock.path.object('methodOverride','methodOverrid
    

Created on Jun 13, 2017

    tutorials : https://www.slideshare.net/hosunglee948/python-52222334
    https://www.internalpointers.com/post/run-painless-test-suites-python-unittest
    
@author: jungh
'''

import unittest
from unittest.mock import MagicMock

from services.watcher_service import WatcherSrv 

class TestModule1(unittest.TestCase):


    def setUp(self):
        self.watcher_service = WatcherSrv()
        self.thing.methodx=MagicMock(return_value=3)
        pass


    def tearDown(self):
        # del self.bag
        pass

    def test_1(self):
        expected=""
        result=self.watcher_service.getReport()
        self.assertEqual(expected, result, "report is wrong")
    
    #@unittest.skipIf(condition, reason)...also on class
    @unittest.skip("demonstrating skipping")    
    def testName(self):
        self.assertTrue(True)
        pass
    
    @unittest.skip("exception test example") 
    def testException(self):
        p=1
        with self.assertRaises(Exception) as context:
            p=p+1#testcode here, 여기에서 예외가 발생하도록 한다 if not ...: raise Exception('msg')
        self.assertEqual('expected', context.exception, 'msg')
        
class TestModule2(unittest.TestCase):


    def setUp(self):
        self.watcher_service = WatcherSrv()
        
        pass


    def tearDown(self):
        # del self.bag
        pass

    def test_1(self):
        expected=""
        result=self.watcher_service.getReport()
        self.assertEqual(expected, result, "report is wrong")
    
    #@unittest.skipIf(condition, reason)...also on class
    @unittest.skip("demonstrating skipping")    
    def testName(self):
        self.assertTrue(True)
        pass




if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    
    testSuite1=unittest.TestSuite()
    testSuite1.addTest(TestModule1())
    testSuite1.addTest(TestModule2())
    runner=unittest.TextTestRunner()
    runner.run(testSuite1)
    
    #or unittest.main()